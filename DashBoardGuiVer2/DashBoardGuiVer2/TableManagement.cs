﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DashBoardGuiVer2
{
    class TableManagement
    {
        public static List<ListViewItem> createSystemTestSummaryTable(IEnumerable<TestSummary> temptestSummary, IEnumerable<BaseUrl> baseUrl, IEnumerable<Class> className, int typeIn)
        {
            List<SummaryAll> tempListSummary = DbHelperMethod.convertDaoToSummaryListAll(temptestSummary, baseUrl, className);
            List<ListViewItem> final = new List<ListViewItem>();
            

            for(int i = 0;i<tempListSummary.Count;++i)
            {                
                ListViewItem lvi = new ListViewItem((i+1).ToString());
                lvi.SubItems.Add(tempListSummary[i].ClassId);
                lvi.SubItems.Add(tempListSummary[i].BaseUrl);
                lvi.SubItems.Add(tempListSummary[i].Success.ToString());
                lvi.SubItems.Add(tempListSummary[i].EventName);
                lvi.SubItems.Add(tempListSummary[i].AverageExecTime.ToString() + " ms");
                lvi.SubItems.Add(tempListSummary[i].NumberOfEvents.ToString());
                if(typeIn==-1)
                {
                    final.Add(lvi);
                }
                else if (tempListSummary[typeIn].TestId == tempListSummary[i].TestId)
                {
                    final.Add(lvi);
                }
            }
            return final;
        }

        public static List<ListViewItem> createSystemTestResultTable(IEnumerable<Test> testTable,IEnumerable< Project> projectTable,IEnumerable<TestType> testType)
        {
            List<ListViewItem> final = new List<ListViewItem>();
            
            int i = 0;
            foreach (Test tempList in testTable)
            {
                Project project = DbHelperMethod.getProject(tempList.ProjectId, projectTable);
                ++i;
                ListViewItem lvi = new ListViewItem(i.ToString());//1
                lvi.SubItems.Add(project.Name);//2
                lvi.SubItems.Add(project.Description);//3
                lvi.SubItems.Add(DbHelperMethod.getTestType(tempList.TestTypeId, testType).Name.ToString());//4
                lvi.SubItems.Add(tempList.BeginTime.ToString());//5
                lvi.SubItems.Add(tempList.EndTime.ToString());//6
                Double average = tempList.TotalApiCallsDuration / tempList.TotalNumberOfApiCalls;
                lvi.SubItems.Add(average.ToString()+" ms");//17

                lvi.SubItems.Add(tempList.NumberOfProcesses.ToString());//7
                lvi.SubItems.Add(tempList.NumberOfEvents.ToString());//8
                lvi.SubItems.Add(tempList.NumberOfTestCaseExecs.ToString());//9
                lvi.SubItems.Add(tempList.NumberOfFailedTestCaseExecs.ToString());//10
                lvi.SubItems.Add(tempList.NumberOfApiCalls.ToString());//11
                lvi.SubItems.Add(tempList.NumberOfFailedApiCalls.ToString());//12
                lvi.SubItems.Add(tempList.TotalApiCallsDuration.ToString() + " ms");//12
                lvi.SubItems.Add(tempList.TotalFailedApiCallsDuration.ToString());//14
                lvi.SubItems.Add(tempList.DelayAfterCall.ToString() + " ms");//15
                if(!String.IsNullOrWhiteSpace(tempList.Comment))
                {
                    lvi.SubItems.Add(tempList.Comment);//16
                }
                final.Add(lvi);
            }
            return final;
        }

        public static List<ListViewItem> createSystemEventTable(IEnumerable<SystemEvent> testTable, 
            IEnumerable<EventType> eventType,
            IEnumerable<Class> className, 
            IEnumerable<BaseUrl> baseUrl,
            IEnumerable<SystemProcess> systemProcess)
        {
            List<ListViewItem> final = new List<ListViewItem>();
            int i = 0;
            foreach (SystemEvent a in testTable)
            {
                ++i;
                ListViewItem lvi = new ListViewItem(i.ToString());//1
                lvi.SubItems.Add(a.TimeStamp.ToString());//2
                lvi.SubItems.Add(DbHelperMethod.getEventType(a.EventTypeId, eventType));//3
                if (a.EventTypeId >= 1000)
                {
                    lvi.BackColor = Color.Orange;
                }
                else 
                {
                    lvi.BackColor = Color.LightBlue;
                }
                SystemProcess proces = DbHelperMethod.getSystemProcess(a.SystemProcessId, systemProcess);
                lvi.SubItems.Add(proces.MachineName);//4
                lvi.SubItems.Add(DbHelperMethod.getClassName(a.ClassId,className));//5
                lvi.SubItems.Add(a.Success.ToString());//6
                lvi.SubItems.Add(a.Name.ToString());//7
                lvi.SubItems.Add(DbHelperMethod.getBaseUrl(a.BaseUrlId,baseUrl));//8
                if (!String.IsNullOrWhiteSpace(a.ExecutionTime.ToString()))
                lvi.SubItems.Add(a.ExecutionTime.ToString() + " ms");//9                
                lvi.SubItems.Add(a.InputData);//10
                lvi.SubItems.Add(a.OutputData);//11
                lvi.SubItems.Add(a.ErrorMsg);//12
                lvi.SubItems.Add(a.ExceptionMsg);//13
                final.Add(lvi);
            }
            return final;
        }
    }
}

