﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DashBoardGuiVer2
{
    public partial class Form1 : Form
    {
        DashboardDataContext dataContext;
        IEnumerable<TestSummary> testSummary;
        IEnumerable<Project> project;
        IEnumerable<EventType> eventType;
        IEnumerable<Class> className;
        IEnumerable<BaseUrl> baseUrl;
        IEnumerable<Test> testTable;
        IEnumerable<TestType> testType;
        IEnumerable<SystemEvent> systemEvent;
        IEnumerable<SystemProcess> systemProcess;
        Boolean inProgress;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "Dashboard tool";
            inProgress = true;
            connect();
            loadDB();
            createFinalTestSummaryTable();
            createFinalTestResultTable();
            createFinalSystemEventTable();
            inProgress = false;

        }
        public void showMyDialogBox(int x)
        {
            FormDialgogGetText testDialog = new FormDialgogGetText();

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
              MessageBox.Show("Added text: "+testDialog.getTextFromDialogComment()+" to the position number "+x.ToString());
            }
            else
            {
                MessageBox.Show("cancel");
            }
            testDialog.Dispose();
        }
       
        public void createFinalSystemEventTable()
        {
            List<ListViewItem> lvi = TableManagement.createSystemEventTable(systemEvent, eventType, className,baseUrl,systemProcess);
            addListToGui(lvi, listViewSystemEvent, AppConstants.headerForSystemEvent.ToList());
        }

        public void setHeaderForTable(List<String> headerForTestTable,ListView view)
        {
            view.Columns.Clear();
            foreach (String a in headerForTestTable)
            {
                view.Columns.Add(a);
            }
        }
        

        public void createFinalTestResultTable()
        {
            List<ListViewItem> lvi = TableManagement.createSystemTestResultTable(testTable,project,testType);
            addListToGui(lvi, listViewTest, AppConstants.headerForTestTable.ToList());
        }

        public void createFinalTestSummaryTable()
        { 
            List<ListViewItem> lvi = TableManagement.createSystemTestSummaryTable(testSummary, baseUrl, className,-1);
            addListToGui(lvi, listView3, AppConstants.headerForSummary.ToList());
        }

        public void addListToGui(List<ListViewItem> container, ListView view,List<String> header)
        {
            setHeaderForTable(header, view);
            foreach(ListViewItem a in container)
            {
                view.Items.Add(a);
            }
            view.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            view.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
        
        public void connect()
        {        
            dataContext = new DashboardDataContext(AppConstants.connStr);
        }
        public void loadDB()
        {
            testSummary = from se in dataContext.TestSummaries
                          select se;

            eventType = from se in dataContext.EventTypes
                        select se;

            className = from se in dataContext.Classes
                        select se;

            baseUrl = from se in dataContext.BaseUrls
                      select se;

            testTable = from se in dataContext.Tests
                        select se;

            systemEvent = from se in dataContext.SystemEvents
                          select se;
            project = from se in dataContext.Projects
                          select se;
            testType = from se in dataContext.TestTypes
                      select se;

            systemProcess = from se in dataContext.SystemProcesses
                       select se;

            //foreach (Test tempList in testTable)
            //{
            //    //MessageBox.Show(tempList.BeginTime.ToString());
            //}
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void onclickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (inProgress == false)
            {
                if (listViewTest.SelectedItems.Count != 0)
                {
                    int x = int.Parse(listViewTest.SelectedItems[0].Text);
                   
                        //List<SummaryAll> tempListSummary = DbHelperMethod.convertDaoToSummaryListAll(testSummary, baseUrl, className);
                            //DbHelperMethod.getSummaryAllForTestID(x, tempListSummary);

                            List<ListViewItem> lvi = TableManagement.createSystemTestSummaryTable(testSummary, baseUrl, className,x);
                            FormTestDetails f2 = new FormTestDetails(lvi);                       
                            f2.MinimizeBox = true;                    
                            f2.Show();
                            f2.TopMost = true;                     
                }
            }
            else
            {
                MessageBox.Show("Please wait, loading data");
            }
        }

        private void listView3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void changeCommentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (inProgress == false)
            {
                if (listViewTest.SelectedItems.Count != 0)
                {
                    int x = int.Parse(listViewTest.SelectedItems[0].Text);
                    showMyDialogBox(x);
                }
            }
        }

    }
}
