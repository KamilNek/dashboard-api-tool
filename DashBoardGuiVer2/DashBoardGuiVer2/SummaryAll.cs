﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashBoardGuiVer2
{
    public class SummaryAll
    {
        public SummaryAll(int p1, string p2, string p3, bool p4, string p5, long p6, int p7, long testID)
        {
            this.evntTypeId = p1;
            this.classId = p2;
            this.baseUrl = p3;
            this.success = p4;
            this.eventName = p5;
            this.averageExecTime = p6;
            this.numberOfEvents = p7;
            this.testId = testID;
        }

        long testId;

        public long TestId
        {
            get { return testId; }
            set { testId = value; }
        }


        private int evntTypeId;//1

        public int EvntTypeId
        {
            get { return evntTypeId; }
            set { evntTypeId = value; }
        }

        private String classId;//2

        public String ClassId
        {
            get { return classId; }
            set { classId = value; }
        }

        private bool success;//4

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        private string eventName;//5

        public string EventName
        {
            get { return eventName; }
            set { eventName = value; }
        }

        private long averageExecTime;//6

        public long AverageExecTime
        {
            get { return averageExecTime; }
            set { averageExecTime = value; }
        }

        private int numberOfEvents;//7

        public int NumberOfEvents
        {
            get { return numberOfEvents; }
            set { numberOfEvents = value; }
        }

        private String baseUrl;//3

        public String BaseUrl
        {
            get { return baseUrl; }
            set { baseUrl = value; }
        }
    }
}
