﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashBoardGuiVer2
{
    public class DbHelperMethod
    {
        public static String getEventType(long typeIn,IEnumerable<EventType> eventType)
        {
            foreach (EventType a in eventType)
            {
                if (a.Id == typeIn)
                {
                    return a.Name.ToString();
                }
            }
            return "";
        }
        public static Project getProject(long typeIn, IEnumerable<Project> projectTable)
        {
            foreach (Project a in projectTable)
            {
                if (a.Id == typeIn)
                {
                    return a;
                }
            }
            return null;
        }

        public static Test getTest(long typeIn, IEnumerable<Test> testTable)
        {
            foreach (Test a in testTable)
            {
                if (a.Id == typeIn)
                {
                    return a;
                }
            }
            return null;
        }

        public static TestType getTestType(long typeIn, IEnumerable<TestType> testTable)
        {
            foreach (TestType a in testTable)
            {
                if (a.Id == typeIn)
                {
                    return a;
                }
            }
            return null;
        }

        public static TestSummary getTestSummary(int typeIn, IEnumerable<TestSummary> testSummary)
        {
            int i = 0;
            foreach (TestSummary a in testSummary)
            {
                ++i;
                if (i == typeIn)
                {
                    return a;
                }
            }
            return null;
        }

        public static List<SummaryAll> getSummaryAllForTestID(int typeIn, List<SummaryAll> tempListSummary)
        {
            List<SummaryAll> willSend = new List<SummaryAll>();

            for (int i = 0; i < tempListSummary.Count(); ++i)
            {
                if (tempListSummary[typeIn].TestId == tempListSummary[i].TestId)
                {
                    willSend.Add(tempListSummary[i]);
                }
            }
            return willSend;
        }

        public static String getBaseUrl(System.Nullable<long> typeIn,IEnumerable<BaseUrl> baseUrl)
        {
            foreach (BaseUrl a in baseUrl)
            {
                if (a.Id == typeIn)
                {
                    return a.Address.ToString();
                }
            }
            return "";
        }

        public static String getClassName(long typeIn,IEnumerable<Class> className)
        {
            foreach (Class a in className)
            {
                if (a.Id == typeIn)
                {
                    return a.Name;
                }
            }
            return "";
        }

        public static SystemProcess getSystemProcess(long typeIn, IEnumerable<SystemProcess> className)
        {
            foreach (SystemProcess a in className)
            {
                if (a.Id == typeIn)
                {
                    return a;
                }
            }
            return null;
        }

        public static List<SummaryAll> convertDaoToSummaryListAll(IEnumerable<TestSummary> temptestSummary, IEnumerable<BaseUrl> baseUrl,IEnumerable<Class> className)
        {
            List<TestSummary> listTestSummary = temptestSummary.ToList();
            List<SummaryAll> final = new List<SummaryAll>();
            foreach (TestSummary a in listTestSummary)
            {
                final.Add(new SummaryAll((a.EventTypeId),//1
                getClassName(a.ClassId,className),//2
                getBaseUrl(a.BaseUrlId,baseUrl),//3
                a.Success,//4
                a.EventName.ToString(),//5
                a.AverageExecTime,//6
                a.NumberOfEvents,
                a.TestId));//7
            }
            return final;
        }
    }
}
