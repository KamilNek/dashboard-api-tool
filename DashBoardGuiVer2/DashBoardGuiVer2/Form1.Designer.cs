﻿namespace DashBoardGuiVer2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listViewTest = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStripTest = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.onclickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeCommentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listViewSystemEvent = new System.Windows.Forms.ListView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listView3 = new System.Windows.Forms.ListView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.contextMenuStripTest.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(848, 324);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listViewTest);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(840, 295);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "List of tests performed";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listViewTest
            // 
            this.listViewTest.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.listViewTest.ContextMenuStrip = this.contextMenuStripTest;
            this.listViewTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewTest.FullRowSelect = true;
            this.listViewTest.GridLines = true;
            this.listViewTest.HideSelection = false;
            this.listViewTest.Location = new System.Drawing.Point(3, 3);
            this.listViewTest.MultiSelect = false;
            this.listViewTest.Name = "listViewTest";
            this.listViewTest.Size = new System.Drawing.Size(834, 289);
            this.listViewTest.TabIndex = 0;
            this.listViewTest.UseCompatibleStateImageBehavior = false;
            this.listViewTest.View = System.Windows.Forms.View.Details;
            this.listViewTest.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // contextMenuStripTest
            // 
            this.contextMenuStripTest.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStripTest.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onclickToolStripMenuItem,
            this.changeCommentToolStripMenuItem});
            this.contextMenuStripTest.Name = "contextMenuStrip1";
            this.contextMenuStripTest.Size = new System.Drawing.Size(196, 80);
            // 
            // onclickToolStripMenuItem
            // 
            this.onclickToolStripMenuItem.Name = "onclickToolStripMenuItem";
            this.onclickToolStripMenuItem.Size = new System.Drawing.Size(195, 24);
            this.onclickToolStripMenuItem.Text = "Show summary";
            this.onclickToolStripMenuItem.Click += new System.EventHandler(this.onclickToolStripMenuItem_Click);
            // 
            // changeCommentToolStripMenuItem
            // 
            this.changeCommentToolStripMenuItem.Name = "changeCommentToolStripMenuItem";
            this.changeCommentToolStripMenuItem.Size = new System.Drawing.Size(195, 24);
            this.changeCommentToolStripMenuItem.Text = "Change comment";
            this.changeCommentToolStripMenuItem.Click += new System.EventHandler(this.changeCommentToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(840, 295);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "List of SystemEvent";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listViewSystemEvent);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(834, 289);
            this.panel1.TabIndex = 0;
            // 
            // listViewSystemEvent
            // 
            this.listViewSystemEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSystemEvent.FullRowSelect = true;
            this.listViewSystemEvent.GridLines = true;
            this.listViewSystemEvent.Location = new System.Drawing.Point(0, 0);
            this.listViewSystemEvent.Name = "listViewSystemEvent";
            this.listViewSystemEvent.Size = new System.Drawing.Size(834, 289);
            this.listViewSystemEvent.TabIndex = 0;
            this.listViewSystemEvent.UseCompatibleStateImageBehavior = false;
            this.listViewSystemEvent.View = System.Windows.Forms.View.Details;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(840, 295);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "List of summary";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.listView3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(834, 289);
            this.panel2.TabIndex = 0;
            // 
            // listView3
            // 
            this.listView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView3.FullRowSelect = true;
            this.listView3.GridLines = true;
            this.listView3.Location = new System.Drawing.Point(0, 0);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(834, 289);
            this.listView3.TabIndex = 0;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            this.listView3.SelectedIndexChanged += new System.EventHandler(this.listView3_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 324);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.contextMenuStripTest.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listViewTest;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTest;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripMenuItem onclickToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ToolStripMenuItem changeCommentToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView listViewSystemEvent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView listView3;
    }
}

