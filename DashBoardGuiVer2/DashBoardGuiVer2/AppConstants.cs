﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashBoardGuiVer2
{
    public class AppConstants
    {
        public const string server = "L530-LENOVO\\SQLEXPRESS";
        public const string database = "FitcijnDashboardDB";
        public const string user = "sa";
        public const string password = "Kotki123";

        //public const string server = "vgive4a4eh.database.windows.net";
        //public const string database = "fitcijndashboarddb";
        //public const string user = "fitcijn";
        //public const string password = "#Rafal07";

        public const string connStr = "Data Source=" + server +
                         ";Initial Catalog=" + database +
                         ";User ID=" + user +
                         ";Password=" + password;

        public static readonly IList<String> headerForTestTable = new System.Collections.ObjectModel.ReadOnlyCollection<string>
            (new List<String> { 
         "Lp",//1
"Project name",//2
"Project Description",//3
"Test type",//4
"Begin time",//5
"End time",//6
"Average",//17
"Number of processes",//7
"Number of events",//8
"Number of test case execs",//9
"Number of failed test case execs",//10
"Number of api calls",//11
"Number of failed api calls",//12
"Total Number of api calls duration",//13
"Total Number of failed api calls duration",//14
"Delay after calls",//15
"Comment"//16            
            });

        public static readonly IList<String> headerForSummary = new System.Collections.ObjectModel.ReadOnlyCollection<string>
            (new List<String> { 
         "Lp","Responsible class","Server address","Success","Type of test","Average time","Number of event"});

        public static readonly IList<String> headerForSystemEvent = new System.Collections.ObjectModel.ReadOnlyCollection<string>
            (new List<String> { 
        "Lp",//1
"TimeStamp",//2
"eventType",//3
"SystemProcessId",//4
"ClassName",//5
"Success",//6
"Name",//7
"getBaseUrl",//8
"ExecutionTime",//9
"Input data",//10
"Output data",//11
"Error Msg",//12
"Exception Msg"//13
            });
    }
}
