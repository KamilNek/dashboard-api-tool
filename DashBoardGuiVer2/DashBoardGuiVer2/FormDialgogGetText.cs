﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DashBoardGuiVer2
{
    public partial class FormDialgogGetText : Form
    {
        public FormDialgogGetText()
        {
            InitializeComponent();
        }
        public String getTextFromDialogComment()
        {
            return textBox1.Text;
        }

        private void FormDialgogGetText_Load(object sender, EventArgs e)
        {
            this.Text = "Enter the text comment";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("Please complete the field");
            }
            else
            this.DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
