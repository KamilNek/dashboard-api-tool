﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DashBoardGuiVer2
{
    public partial class FormTestDetails : Form
    {
        public List<ListViewItem> lvi;
        public FormTestDetails(List<ListViewItem> lista)
        {
            InitializeComponent();
            this.lvi = lista;   
        }

        private void FormTestDetails_Load(object sender, EventArgs e)
        {
            this.Text = "Summary";
            addListToGui(lvi, listView1, AppConstants.headerForSummary.ToList());
        }

        public void addListToGui(List<ListViewItem> container, ListView view, List<String> header)
        {
            setHeaderForTable(header, view);
            foreach (ListViewItem a in container)
            {
                view.Items.Add(a);
            }
            view.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            view.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        public void setHeaderForTable(List<String> header, ListView view)
        {
            view.Columns.Clear();
            foreach (String a in header)
            {
                view.Columns.Add(a);
            }
        }
    }
}
